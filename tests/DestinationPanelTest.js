import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import DestinationPanel from '../src/DestinationPanel.jsx';

module.exports =
describe('DestinationPanel Component', function() {
  var renderer;
  beforeEach( function() {
    renderer = TestUtils.createRenderer();
  } );

  var fakeCallback = function() {};

  var fakeDestination  = {id: '1', name: 'destination 1', email: 'destination1@gmail.com', docs: 40, envelopes: [
    {id:'11', name: 'envelope1', sentDate: null, docs: [
      {id:'11', envelopeId: '11', form_name: 'Anthem PPO 1345 From for Eric Kish', name:'Eric Kish'},
      {id:'12', envelopeId: '11', form_name: 'Anthem PPO 1345 From for Ignacio', name:'Ignacio Fernandez'}
    ]
    },
    {id:'12', name: 'envelope2', sentDate: '2016-01-08T12:48:39+00:00', docs: [{id:'21', envelopeId: '12', form_name: 'Anthem PPO 1345 From for Eric Kish', name:'Eric Kish'}] }
  ],
    postpone: []
  };

  it( 'Render', function() {
    var result;
    renderer.render( <DestinationPanel key={fakeDestination.id} eventKey={'e_'+fakeDestination.id} destination={fakeDestination} /> );
    result = renderer.getRenderOutput();
    expect( result.props.className ).to.equal( 'destination-header' );
  });

  it( 'Render SentView', function() {
    var tree = TestUtils.renderIntoDocument(<DestinationPanel key={fakeDestination.id} eventKey={'e_'+fakeDestination.id} destination={fakeDestination} /> );
    var headers = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'envelope-header' );

    expect(headers.length).to.equal( 2 );
  });

  it( 'Render OutBoxView', function() {
    var tree = TestUtils.renderIntoDocument(<DestinationPanel outBoxView key={fakeDestination.id}
                                                              eventKey={'e_'+fakeDestination.id} destination={fakeDestination}
                                                              addEnvelope={fakeCallback} removeDestination={fakeCallback}
                                                              showEnvelopeModal={fakeCallback} showPostponeModal={fakeCallback} /> );

    var headers = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'envelope-header' );

    expect(headers.length).to.equal( 1 );
  });

});
