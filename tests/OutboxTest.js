import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import Outbox from '../src/Outbox.jsx';

module.exports =
describe('Outbox Component', function() {
  var renderer;
  beforeEach( function() {
    renderer = TestUtils.createRenderer();
  } );

  var fakeCallback = function() {};

  var fakeDestinations = [
    {id: '1', name: 'destination 1', email: 'destination1@gmail.com', docs: 40, envelopes: [
      {id:'11', name: 'envelope1', sentDate: null, docs: [
        {id:'11', envelopeId: '11', form_name: 'Anthem PPO 1345 From for Eric Kish', name:'Eric Kish'},
        {id:'12', envelopeId: '11', form_name: 'Anthem PPO 1345 From for Ignacio', name:'Ignacio Fernandez'}
      ]
      },
      {id:'12', name: 'envelope2', sentDate: '2016-01-08T12:48:39+00:00', docs: [{id:'21', envelopeId: '12', form_name: 'Anthem PPO 1345 From for Eric Kish', name:'Eric Kish'}] }
    ],
      postpone: []
    }
  ];

  it( 'Render Sent View', function() {
    var result;
    renderer.render(  <Outbox destinationsList={fakeDestinations} onSendEnvelope={fakeCallback}
                              onUpdateEnvelope={fakeCallback} onDeleteEnvelope={fakeCallback}
                              showEnvelopeModal={fakeCallback} showPostponeModal={fakeCallback}
                              removeDestination={fakeCallback} addEnvelope={fakeCallback} /> );
    result = renderer.getRenderOutput();
    expect( result.props.className ).to.equal( 'outbox sent' );
  });

  it( 'Render Outbox View', function() {
    var result;
    renderer.render(  <Outbox outBoxView destinationsList={fakeDestinations} onSendEnvelope={fakeCallback}
                              onUpdateEnvelope={fakeCallback} onDeleteEnvelope={fakeCallback}
                              showEnvelopeModal={fakeCallback} showPostponeModal={fakeCallback}
                              removeDestination={fakeCallback} addEnvelope={fakeCallback} /> );
    result = renderer.getRenderOutput();
    expect( result.props.className ).to.equal( 'outbox' );
  });

});
