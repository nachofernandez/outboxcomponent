import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import Envelope from '../src/Envelope.jsx';

module.exports =
describe('Envelope Component', function() {
  var renderer;
  beforeEach( function() {
    renderer = TestUtils.createRenderer();
  } );

  var fakeEnvelope  = {id:'11', name: 'envelope1', sentDate: null, docs: []};

  it( 'Render sent envelope', function() {
    var result;
    renderer.render(  <Envelope sent key={fakeEnvelope.id} eventKey={fakeEnvelope.id} envelope={fakeEnvelope}/> );
    result = renderer.getRenderOutput();
    expect( result.props.className ).to.equal( 'envelope-header' );
  });

  it( 'Render not sent envelope', function() {
    var fakeFunction = function() {};
    var result;
    renderer.render(  <Envelope key={fakeEnvelope.id} eventKey={fakeEnvelope.id} envelope={fakeEnvelope}
                                showEnvelopeModal={fakeFunction} sendCallback={fakeFunction}
                                deleteCallback={fakeFunction} updateCallback={fakeFunction}/> );
    result = renderer.getRenderOutput();
    expect( result.props.className ).to.equal( 'envelope-header' );
  });

  it( 'Show envelope modal', function() {
    var fakeFunction = function() {};

    var envelopeModal = function(envelope) {
      expect(envelope).to.exist;
      expect(envelope.id).to.equal('11');
    };

    var tree = TestUtils.renderIntoDocument(  <Envelope key={fakeEnvelope.id} eventKey={fakeEnvelope.id} envelope={fakeEnvelope}
                                                        showEnvelopeModal={envelopeModal} sendCallback={fakeFunction}
                                                        deleteCallback={fakeFunction} updateCallback={fakeFunction}/> );

    TestUtils.Simulate.click( ReactDOM.findDOMNode( TestUtils.findRenderedDOMComponentWithClass( tree, 'envelope-btn' ) ) );
  });

  it( 'Delete envelope', function() {
    var fakeFunction = function() {};

    var deleteCallback = function(envelope) {
      expect(envelope).to.exist;
      expect(envelope.id).to.equal('11');
    };

    var tree = TestUtils.renderIntoDocument(  <Envelope key={fakeEnvelope.id} eventKey={fakeEnvelope.id} envelope={fakeEnvelope}
                                                        showEnvelopeModal={fakeFunction} sendCallback={fakeFunction}
                                                        deleteCallback={deleteCallback} updateCallback={fakeFunction}/> );

    TestUtils.Simulate.click( ReactDOM.findDOMNode( TestUtils.findRenderedDOMComponentWithClass( tree, 'delete-btn' ) ) );
  });

});
