import React from 'react';
import { TextWidget, SelectWidget, DateWidget, NumberWidget, RadioWidget } from '../lib/ReactQueryBuilder';

export default {
  conjunctions: {
    and: {
      label: 'And',
      value: (value) => value.size > 1 ? `(${value.join(' AND ')})` : value.first()
    },
    or: {
      label: 'Or',
      value: (value) => value.size > 1 ? `(${value.join(' OR ')})` : value.first()
    }
  },
  fields: {
    age: {
      label: 'Age',
      widget: 'number',
      operators: ['greaterEqualsThan','greaterThan','equals','lowerThan','lowerEqualsThan']
    },
    groupMembership: {
      label: 'Group membership',
      widget: 'select',
      options: {
        group1: 'Group 1',
        group2: 'Group 2',
        group3: 'Group 3'
      },
      operators: ['is','isNot']
    },
    person: {
      label: 'Person',
      widget: 'radio',
      defaultOption: 'active',
      options: {
        active: 'Active',
        inactive: 'Inactive'
      },
      operators: ['is','isNot']
    },
    self_service: {
        label: 'Self Service',
        widget: 'radio',
        defaultOption: 'on',
        options: {
          on: 'ON',
          pending: 'Pending',
          off: 'OFF'
        },
        operators: ['is','isNot']
    },
    visibility: {
      label: 'Visibility',
      widget: 'radio',
      defaultOption: 'on',
      options: {
        on: 'ON',
        off: 'OFF'
      },
      operators: ['is','isNot']
    },
    state: {
      label: 'Home State',
      widget: 'select',
      options: {
        alabama: 'Alabama',
        alaska: 'Alaska',
        arizona: 'Arizona',
        arkansas: 'Arkansas',
        california: 'California',
        colorado: 'Colorado',
        connecticut: 'Connecticut',
        delaware: 'Delaware',
        florida: 'Florida',
        georgia: 'Georgia',
        hawaii: 'Hawaii',
        idaho: 'Idaho',
        illinois: 'Illinois',
        indiana: 'Indiana',
        iowa: 'Iowa',
        kansas: 'Kansas',
        kentucky: 'Kentucky',
        louisiana: 'Louisiana',
        maine: 'Maine',
        maryland: 'Maryland',
        massachusetts: 'Massachusetts',
        michigan: 'Michigan',
        minnesota: 'Minnesota',
        mississippi: 'Mississippi',
        missouri: 'Missouri',
        montana: 'Montana',
        nebraska: 'Nebraska',
        nevada: 'Nevada',
        new_hampshire: 'New Hampshire',
        new_jersey:  'New Jersey',
        new_mexico: 'New Mexico',
        new_york: 'New York',
        north_carolina: 'North Carolina',
        north_dakota: 'North Dakota',
        ohio: 'Ohio',
        oklahoma: 'Oklahoma',
        oregon: 'Oregon',
        pennsylvania: 'Pennsylvania',
        rhode_island: 'Rhode Island',
        south_carolina: 'South Carolina',
        south_dakota: 'South Dakota',
        tennessee: 'Tennessee',
        texas: 'Texas'
      },
      operators: ['is','isNot']
    }

  },
  operators: {
    equals: {
      label: 'Is equal to',
      value: (value, field) => `${field}=${value.first()}`
    },
    greaterThan: {
      label: 'Is greater than',
      value: (value, field) => `${field}>${value.first()}`
    },
    greaterEqualsThan: {
      label: 'Is equal or greater than',
      value: (value, field) => `${field}>=${value.first()}`
    },
    lowerThan: {
      label: 'Is lower than',
      value: (value, field) => `${field}<${value.first()}`
    },
    lowerEqualsThan: {
      label: 'Is equal or lower than',
      value: (value, field) => `${field}<=${value.first()}`
    },
    is: {
      label: 'Is',
      value: (value, field) => `${field} IS ${value.first()}`
    },
    isNot: {
      label: 'Is not',
      value: (value, field) => `${field} IS NOT ${value.first()}`
    },
    contains: {
      label: 'Contains',
      value: (value, field) => `${field}:*${value.first()}*`
    },
    startsWith: {
      label: 'Starts with',
      value: (value, field) => `${field}:${value.first()}*`
    },
    endsWith: {
      label: 'Ends with',
      value: (value, field) => `${field}:*${value.first()}`
    },
    exactPhrase: {
      label: 'Exact phrase',
      value: (value, field) => `${field}:"${value.first()}"`
    },
    termsOne: {
      label: 'At least one of the words',
      value: (value, field) => `${field}:(${value.first().trim().split(' ').join(' OR ')})`
    },
    termsAll: {
      label: 'All of the words',
      value: (value, field) => `${field}:(${value.first().trim().split(' ').join(' AND ')})`
    },
    termsNone: {
      label: 'Without the words',
      value: (value, field) => `-${field}:(${value.first().trim().split(' ').join(' OR ')})`
    }
  },
  widgets: {
    radio: {
      factory: (props) => <RadioWidget {...props} />
    },
    number: {
      factory: (props) => <NumberWidget {...props} />
    },
    text: {
      factory: (props) => <TextWidget {...props} />
    },
    select: {
      factory: (props) => <SelectWidget {...props} />
    },
    date: {
      factory: (props) => <DateWidget {...props} />
    }
  },
  settings: {
    maxNesting: 10
  }
};
