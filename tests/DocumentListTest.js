import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import DocumentList from '../src/DocumentList.jsx';

module.exports =
describe('DocumentList Component', function() {
  var renderer;
  beforeEach( function() {
    renderer = TestUtils.createRenderer();
  } );

  var fakeDocs  = [
    {id:'31', envelopeId: '21', form_name: 'Anthem PPO 1345 From for Eric Kish', name:'Eric Kish'},
    {id:'32', envelopeId: '21', form_name: 'Anthem PPO 1345 From for Ignacio', name:'Ignacio Fernandez'}
  ];

  it('Render', function() {
    var result;
    var fakeFunction = function() {};
    renderer.render( <DocumentList viewType="envelope" actionCallback={fakeFunction} docs={fakeDocs} /> );
    result = renderer.getRenderOutput();

    expect( result.type ).to.equal( 'div' );
  });

  it('Check list items', function() {
    var fakeFunction = function() {};

    var tree = TestUtils.renderIntoDocument(<DocumentList viewType="envelope"  docs={fakeDocs}
                                                          removeDocument={fakeFunction}
                                                          actionCallback={fakeFunction} /> );

    var items = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'col-md-12 document-item' );

    expect( items.length ).to.equal( 2 );
  });



});
