import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import PostponeRow from '../src/PostponeRow.jsx';

module.exports =
describe('PostponeRow Component', function() {
  var renderer;
  beforeEach( function() {
    renderer = TestUtils.createRenderer();
  } );

  var fakeDestination  = {id: '1', name: 'destination 1', email: 'destination1@gmail.com', docs: 40,
    envelopes: [
      {id:'11', name: 'envelope1', sentDate: null, docs: []}
    ],
    postpone: []
  };

  it( 'Render', function() {
    var result;
    var postponeModal = function() {};
    renderer.render( <PostponeRow destination={fakeDestination} showPostponeModal={postponeModal}/> );
    result = renderer.getRenderOutput();
    expect( result.props.className ).to.equal( 'row postponed-row' );
  });

  it( 'Click on docs link', function() {
    var postponeModal = function(dest) {
      expect(dest).to.exist;
    };

    var tree = TestUtils.renderIntoDocument(  <PostponeRow destination={fakeDestination} showPostponeModal={postponeModal}/> );

    TestUtils.Simulate.click( ReactDOM.findDOMNode( TestUtils.findRenderedDOMComponentWithClass( tree, 'btn-link' ) ) );

  });

});
