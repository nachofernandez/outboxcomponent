import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import DocumentItem from '../src/DocumentItem.jsx';

module.exports =
describe('DocumentItem Component', function() {
  var renderer;
  beforeEach( function() {
    renderer = TestUtils.createRenderer();
  } );

  var fakeDocument  = {id:'12', envelopeId: '11', form_name: 'Anthem PPO 1345 From for Ignacio', name:'Ignacio Fernandez'};

  it( 'Render not selected document', function() {
    var result;
    var fakeFunction = function() {};
    var selected = null;
    var viewType = 'envelope';

    renderer.render(<DocumentItem selected={selected} key={fakeDocument.id}
                                  document={fakeDocument}
                                  viewType={viewType}
                                  removeDocument={fakeFunction}
                                  onClick={fakeFunction}
                                  actionCallback={fakeFunction} />);
    result = renderer.getRenderOutput();
    expect( result.props.className ).to.equal( 'col-md-12 document-item' );
  });

  it( 'Render selected document', function() {
    var result;
    var fakeFunction = function() {};
    var viewType = 'envelope';

    renderer.render(<DocumentItem selected={fakeDocument} key={fakeDocument.id}
                                  document={fakeDocument}
                                  viewType={viewType}
                                  removeDocument={fakeFunction}
                                  onClick={fakeFunction}
                                  actionCallback={fakeFunction} />);
    result = renderer.getRenderOutput();
    expect( result.props.className ).to.equal( 'col-md-12 document-item selected-item' );
  });

  it( 'Selected all', function() {
    var result;
    var fakeFunction = function() {};
    var viewType = 'envelope';
    var selected = {type: 'all'};

    renderer.render(<DocumentItem selected={selected} key={fakeDocument.id}
                                  document={fakeDocument}
                                  viewType={viewType}
                                  removeDocument={fakeFunction}
                                  onClick={fakeFunction}
                                  actionCallback={fakeFunction} />);
    result = renderer.getRenderOutput();
    expect( result.props.className ).to.equal( 'col-md-12 document-item selected-item' );
  });


  it( 'Click Document', function() {
    var onClick = function(doc) {
      expect(doc).to.exist;
    };

    var fakeFunction = function() {};

    var viewType = 'envelope';
    var tree = TestUtils.renderIntoDocument(  <DocumentItem selected={fakeDocument} key={fakeDocument.id}
                                                            document={fakeDocument}
                                                            viewType={viewType}
                                                            removeDocument={fakeFunction}
                                                            onClick={onClick}
                                                            actionCallback={fakeFunction} /> );

    TestUtils.Simulate.click( ReactDOM.findDOMNode( tree) );

  });

  it( 'Delete Document', function() {
    var deleteCallback = function(doc) {
      expect(doc).to.exist;
    };

    var fakeFunction = function() {};

    var viewType = 'envelope';
    var tree = TestUtils.renderIntoDocument(  <DocumentItem selected={fakeDocument} key={fakeDocument.id}
                                                            document={fakeDocument}
                                                            viewType={viewType}
                                                            removeDocument={deleteCallback}
                                                            onClick={fakeFunction}
                                                            actionCallback={fakeFunction} /> );

    TestUtils.Simulate.click( ReactDOM.findDOMNode( TestUtils.findRenderedDOMComponentWithClass( tree, 'delete-option' )) );

  });

  it( 'Call Custom Action', function() {
    var actionCallback = function(doc) {
      expect(doc).to.exist;
    };

    var fakeFunction = function() {};

    var viewType = 'envelope';
    var tree = TestUtils.renderIntoDocument(  <DocumentItem selected={fakeDocument} key={fakeDocument.id}
                                                            document={fakeDocument}
                                                            viewType={viewType}
                                                            removeDocument={fakeFunction}
                                                            onClick={fakeFunction}
                                                            actionCallback={actionCallback} /> );

    TestUtils.Simulate.click( ReactDOM.findDOMNode( TestUtils.findRenderedDOMComponentWithClass( tree, 'custom-option' )) );

  });
});
