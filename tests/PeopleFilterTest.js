import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import config from './config';
import PostponeRow from '../src/PostponeRow.jsx';
import { Query, Builder, Preview, Buttons } from '../lib/ReactQueryBuilder';

module.exports =
describe('People Filter Component', function() {
  var renderer;
  beforeEach( function() {
    renderer = TestUtils.createRenderer();
  } );

  it( 'Render People Filter', function() {
    var result;
    var saveCallBack = function() {};

    renderer.render(
      <Query {...config}>
      {(props) => (
        <div>
          <div className="query-builder">
            <Builder {...props} />
          </div>
          <Buttons {...props} onSave={saveCallBack}/>
        </div>
      )}
    </Query> );
    result = renderer.getRenderOutput();
    expect(result).to.exist;
  });

  it( 'Save Empty Query', function() {
    var saveCallBack = function(params) {
      expect(params).to.not.exist;
    };

    var tree = TestUtils.renderIntoDocument(
      <Query {...config}>
      {(props) => (
        <div>
          <div className="query-builder">
            <Builder {...props} />
          </div>
          <Buttons {...props} onSave={saveCallBack}/>
        </div>
      )}
    </Query>);

    TestUtils.Simulate.click( ReactDOM.findDOMNode( TestUtils.findRenderedDOMComponentWithClass( tree, 'btn-success' ) ) );
  });

  it( 'Add new rule', function() {
    var saveCallBack = function(params) {};

    var tree = TestUtils.renderIntoDocument(
      <Query {...config}>
        {(props) => (
          <div>
            <div className="query-builder">
              <Builder {...props} />
            </div>
            <Buttons {...props} onSave={saveCallBack}/>
          </div>
        )}
      </Query>);

    var rules = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'rule' );
    expect(rules.length).to.equal( 2 );

    TestUtils.Simulate.click( ReactDOM.findDOMNode( TestUtils.findRenderedDOMComponentWithClass( tree, 'action--ADD-RULE' ) ) );

    rules = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'rule' );
    expect(rules.length).to.equal( 3 );
  });

  it( 'Delete rule', function() {
    var saveCallBack = function(params) {};

    var tree = TestUtils.renderIntoDocument(
      <Query {...config}>
        {(props) => (
          <div>
            <div className="query-builder">
              <Builder {...props} />
            </div>
            <Buttons {...props} onSave={saveCallBack}/>
          </div>
        )}
      </Query>);

    var rules = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'rule' );
    expect(rules.length).to.equal( 2 );

    var deleteBtns = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'action--DELETE' );
    TestUtils.Simulate.click( ReactDOM.findDOMNode( deleteBtns[0] ) );

    rules = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'rule' );
    expect(rules.length).to.equal( 1 );
  });

  it( 'Add new group', function() {
    var saveCallBack = function(params) {};

    var tree = TestUtils.renderIntoDocument(
      <Query {...config}>
        {(props) => (
          <div>
            <div className="query-builder">
              <Builder {...props} />
            </div>
            <Buttons {...props} onSave={saveCallBack}/>
          </div>
        )}
      </Query>);

    var groups = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'group' );
    expect(groups.length).to.equal( 1 );

    TestUtils.Simulate.click( ReactDOM.findDOMNode( TestUtils.findRenderedDOMComponentWithClass( tree, 'action--ADD-GROUP' ) ) );

    groups = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'group' );
    expect(groups.length).to.equal( 2 );
  });

  it( 'Delete group', function() {
    var saveCallBack = function(params) {};

    var tree = TestUtils.renderIntoDocument(
      <Query {...config}>
        {(props) => (
          <div>
            <div className="query-builder">
              <Builder {...props} />
            </div>
            <Buttons {...props} onSave={saveCallBack}/>
          </div>
        )}
      </Query>);

    var groups = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'group' );
    expect(groups.length).to.equal( 1 );

    TestUtils.Simulate.click( ReactDOM.findDOMNode( TestUtils.findRenderedDOMComponentWithClass( tree, 'action--ADD-GROUP' ) ) );

    groups = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'group' );
    expect(groups.length).to.equal( 2 );

    var deleteBtns = TestUtils.scryRenderedDOMComponentsWithClass(tree, 'action--DELETE');

    TestUtils.Simulate.click( ReactDOM.findDOMNode( deleteBtns[2] ) );

    groups = TestUtils.scryRenderedDOMComponentsWithClass( tree, 'group' );
    expect(groups.length).to.equal( 1 );
  });


});
