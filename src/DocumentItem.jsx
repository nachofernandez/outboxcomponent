import React from 'react';

class DocumentItem extends React.Component{
  constructor(props) {
    super(props);
  }

  isSelected() {
    return _.isEqual(this.props.selected, {type: 'all'}) || _.isEqual(this.props.selected, this.props.document);
  }

  _menu() {
    if (this.props.viewType === 'envelope') {
      return (
        <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
          <li><a href="#" className="custom-option" onClick={this.props.actionCallback.bind(this, this.props.document)}>Postpone</a></li>
          <li><a href="#" className="delete-option" onClick={this.props.removeDocument.bind(this, this.props.document)}>Delete</a></li>
        </ul>
      );
    } else {
      return (
        <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
          <li><a href="#" className="custom-option" onClick={this.props.actionCallback.bind(this, this.props.document)}>Move to envelope</a></li>
          <li><a href="#" className="delete-option" onClick={this.props.removeDocument.bind(this, this.props.document)}>Delete</a></li>
        </ul>
      );
    }
  }

  _handleClick(e) {
    if (e.target.className.indexOf('not-open') === -1) {
        this.props.onClick(this.props.document);
    }

  }

  render() {
    return (
      <li className={this.isSelected() ? 'col-md-12 document-item selected-item' : 'col-md-12 document-item' } onClick={this._handleClick.bind(this)}>
        <div className="row">
          <div className="col-md-1">
            <i className="sprite sprite-reorder" />
          </div>
          <div className="col-md-4">
            <a className="btn btn-link blue-text" href="#" role="button">
              {this.props.document.form_name}
            </a>
          </div>
          <div className="col-md-offset-1 col-md-4">
            <a className="btn btn-link blue-text" href="#" role="button">
              {this.props.document.name}
            </a>
          </div>
          <div className="col-md-2 pull-right">
            <div className="dropdown pull-right">
              <a id="dropdownMenu1" className="btn btn-link dropdown-toggle not-open" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <span className="not-open">Actions</span>
                <span className="glyphicon glyphicon-menu-down not-open" />
              </a>
              {this._menu()}
            </div>
          </div>
        </div>
      </li>
    );
  }
}

DocumentItem.propTypes = {
  document: React.PropTypes.object,
  selected: React.PropTypes.object,
  removeDocument: React.PropTypes.func,
  actionCallback: React.PropTypes.func,
  onClick:  React.PropTypes.func,
  viewType: React.PropTypes.string
};

module.exports = DocumentItem;
