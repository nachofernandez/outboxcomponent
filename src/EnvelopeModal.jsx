import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import Modal from 'react-bootstrap/lib/Modal';
import DocumentList from './DocumentList.jsx';

class EnvelopeModal extends React.Component{
  constructor(props) {
    super(props);
  }

  _postponeSelected(selected) {
    if (_.isEqual(selected, {type: 'all'})) {
      this.props.envelope.docs.forEach((doc) => {
          this.props.postponeDocument(doc);
      });
    } else {
      this.props.postponeDocument(selected);
    }
  }

  _onHide() {
    this.setState({selected: null});
    this.props.onHide();
  }

  _handleSort(event) {
    this.props.sortCallback(event, this.props.envelope, this.props.destination);
  }


  render() {
    return (
      <Modal {...this.props} dialogClassName="envelope-modal" onHide={this._onHide.bind(this)}>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.envelope ? this.props.envelope.name : ''}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <div className="row">
            <div className="col-md-4">
              <i className="sprite sprite-envelope" />
              <a className="btn btn-link" href="#" role="button">Application forms</a><br />
              <h3>Total Documents in Envelope: {this.props.envelope ? this.props.envelope.docs.length : ''}</h3>
            </div>
            <div className="col-md-4 middle-col">
              <span>{this.props.destination ? this.props.destination.name : ''}</span><br />
              <label>({this.props.destination ? this.props.destination.email : ''})</label>
            </div>
            <div className="col-md-4 last-col">
              <button className="btn btn-success btn-green" type="button">+ Add Document</button>
            </div>
          </div>
          {this.props.envelope ? <DocumentList {...this.props} onSort={this._handleSort.bind(this)} viewType="envelope" actionCallback={this._postponeSelected.bind(this)} docs={this.props.envelope.docs} /> : ''}
        </Modal.Body>
      </Modal>
    );
  }
}


EnvelopeModal.propTypes = {
  envelope: React.PropTypes.object,
  destination: React.PropTypes.object,
  onHide: React.PropTypes.func,
  postponeDocument: React.PropTypes.func,
  sortCallback: React.PropTypes.func
};

module.exports = EnvelopeModal;
