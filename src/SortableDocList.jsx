import React from 'react';
import DocumentItem from './DocumentItem.jsx';
import reactMixin from 'react-mixin';
import SortableMixin from './sortable-mixin';

class SortableDocList extends React.Component{
  constructor(props) {
    super(props);
    this.state = {docs: null}
  }

  handleSort(event) {
    this.props.onSort(event);
  }

  render() {
    var docsList = this.state.docs ? this.state.docs : this.props.docs;
    return (<ul className="documents-list">{
      docsList.map(function(doc) {
      return (
        <DocumentItem {...this.props} key={doc.id} onClick={this.props.onClick} document={doc} />
      );
    }, this)} </ul>);
  }
}

SortableDocList.propTypes = {
  docs: React.PropTypes.array,
  onClick: React.PropTypes.func,
  onSort: React.PropTypes.func
};

reactMixin(SortableDocList.prototype, SortableMixin);

module.exports = SortableDocList;
