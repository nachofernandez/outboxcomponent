import React from 'react';
import Panel from 'react-bootstrap/lib/Panel';
import Envelope from './Envelope.jsx';
import PostponeRow from './PostponeRow.jsx';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';

class DestinationPanel extends React.Component{
  constructor(props) {
    super(props);
  }

  _deleteEnvelope(envelope) {
      this.props.onDeleteEnvelope(this.props.destination, envelope);
  }

  _updateEnvelope(envelope) {
    this.props.onUpdateEnvelope(this.props.destination, envelope);
  }

  _sendEnvelope(envelope) {
    this.props.onSendEnvelope(this.props.destination, envelope);
  }

  _envelopes() {
    return this.props.destination.envelopes.map(function(envelope) {
      if (this.props.outBoxView && !envelope.sentDate) { //In outboxview show not sent envelopes
        return (
          <Envelope {...this.props} key={envelope.id} eventKey={envelope.id} envelope={envelope}
                                    sendCallback={this._sendEnvelope.bind(this)}
                                    deleteCallback={this._deleteEnvelope.bind(this)}
                                    updateCallback={this._updateEnvelope.bind(this)}/>
        );
      } else if (!this.props.outBoxView && envelope.sentDate) {
        return (
          <Envelope {...this.props} sent key={envelope.id} eventKey={envelope.id} envelope={envelope}/>
        );
      }
    }, this);
  }

  _countDocs() {
    var count = 0;
    var envelopes = _.where(this.props.destination.envelopes, {sentDate: null});  //Count not docs in not sent envelopes
    envelopes.forEach(function(envelope){
      count += envelope.docs.length;
    });

    return count;
  }

  _countEnvelopes(sent) {
    var notSendCount = (_.where(this.props.destination.envelopes, {sentDate: null})).length;

    return sent ? this.props.destination.envelopes.length - notSendCount : notSendCount;
  }

  _getHeader() {
    if (this.props.outBoxView) {
      return (
        <div className="row">
          <div className="col-md-4 first-col panel-title">
            <a href="#" className="collapsible-link">{this.props.destination.name}</a><br />
            <span className="email">({this.props.destination.email})</span>
          </div>
          <div className="col-md-4 middle-col">
            <span>{this._countDocs()} docs in {this._countEnvelopes(false)} envelopes</span><br />
            <span>{this.props.destination.postpone.length} postponed</span>
          </div>
          <div className="col-md-3 last-col">
            <DropdownButton bsStyle="link" title="Actions" id={'destId_'+this.props.destination.id}>
              <MenuItem onSelect={this.props.addEnvelope.bind(this, this.props.destination)}>Add Envelope</MenuItem>
              <MenuItem onSelect={this.props.removeDestination.bind(this, this.props.destination)}>Remove
                Destination</MenuItem>
            </DropdownButton>
          </div>
          <div className="col-md-1 arrow-col">
            <a href="#" className="collapsible-link">
              <i className="collapsible-link glyphicon glyphicon-triangle-bottom" aria-hidden="true"/>
              <i className="collapsible-link glyphicon glyphicon-triangle-left" aria-hidden="true"/>
            </a>
          </div>
        </div>
      )
    } else {
      return (
        <div className="row">
          <div className="col-md-4 first-col panel-title">
            <a href="#" className="collapsible-link">{this.props.destination.name}</a><br />
            <span className="email">({this.props.destination.email})</span>
          </div>
          <div className="col-md-4 middle-col"></div>
          <div className="col-md-3 last-col"></div>
          <div className="col-md-1 arrow-col">
            <a href="#" className="collapsible-link">
              <i className="collapsible-link glyphicon glyphicon-triangle-bottom" aria-hidden="true"/>
              <i className="collapsible-link glyphicon glyphicon-triangle-left" aria-hidden="true"/>
            </a>
          </div>
        </div>
      )
    }
  }

  _listHeader() {
    if (!this.props.outBoxView) {
      return (
        <li className="envelope-header">
          <div className="row">
            <div className="col-md-3 first-col"></div>
            <div className="col-md-3 middle-col">
              <span>No of docs</span>
            </div>
            <div className="col-md-3 header">
              <span>Status</span>
            </div>
            <div className="col-md-3 header"></div>
          </div>
        </li>
      )
    }
  }

  render() {
    return (
      <Panel {...this.props} header={this._getHeader()} className="destination-header">
        <ul>
          {this._listHeader()}
          {this._envelopes()}
          {this.props.outBoxView ? <PostponeRow {...this.props} destination={this.props.destination} /> : ''}
        </ul>
      </Panel>
    );
  }
}

DestinationPanel.propTypes = {
  onSelect: React.PropTypes.func,
  eventKey: React.PropTypes.any,
  destination: React.PropTypes.object,
  onSendEnvelope: React.PropTypes.func,
  onDeleteEnvelope: React.PropTypes.func,
  onUpdateEnvelope: React.PropTypes.func,
  addEnvelope: React.PropTypes.func,
  removeDestination: React.PropTypes.func,
  outBoxView: React.PropTypes.bool
};

module.exports = DestinationPanel;
