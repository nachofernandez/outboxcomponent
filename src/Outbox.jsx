import React from 'react';
import Accordion from 'react-bootstrap/lib/Accordion';
import DestinationPanel from './DestinationPanel.jsx'

class Outbox extends React.Component{
  constructor(props) {
    super(props);
  }

  _destinations() {
    return this.props.destinationsList.map(function(destination) {
      return (
        <DestinationPanel {...this.props} key={destination.id} eventKey={'e_'+destination.id} destination={destination} />
      );
    }, this);
  }

  render() {
    return (
      <Accordion className={this.props.outBoxView ? 'outbox' : 'outbox sent'}>
        {this._destinations()}
      </Accordion>
    );
  }
}

Outbox.propTypes = {
  destinationsList: React.PropTypes.array,
  outBoxView: React.PropTypes.bool
};

module.exports = Outbox;
