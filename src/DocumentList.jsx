import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import DocumentItem from './DocumentItem.jsx';
import SortableDocList from './SortableDocList.jsx';

class DocumentList extends React.Component{
  constructor(props) {
    super(props);
    this.state = {selected: null, nameInputVal: '', formNameInputVal: '', docs: null};
  }

  componentWillReceiveProps(nextProps) {
    this._doFilter(nextProps.docs);
  }

  _unSelectAll() {
    this.setState({selected: null});
  }

  _selectAll() {
    this.setState({selected: {type: 'all'}});
  }

  _selectItem(document) {
    if (this.state.selected && _.isEqual(this.state.selected, document)) {
      //IF select the same then unselect
      this.setState({selected: null});
    } else {
      this.setState({selected: document});
    }
  }

  _handleNameChange(event) {
    this.setState({nameInputVal: event.target.value});
  }

  _handleFormNameChange(event) {
    this.setState({formNameInputVal: event.target.value});
  }

  _doFilter(docsList) {
    let searchNameValue = this.state.nameInputVal;
    let searchFormNameValue = this.state.formNameInputVal;

    if (searchNameValue || searchFormNameValue) {
      let filtered = _.filter(docsList, function(doc){
        //Search if the name or form_name contains the strings
        var matchName = true;
        if (searchNameValue) {
          let name =  doc.form_name.toLowerCase();
          matchName = (name.indexOf(searchNameValue.toLowerCase()) > -1);
        }

        var matchForm = true;
        if (searchFormNameValue) {
          let form_name =  doc.form_name.toLowerCase();
          matchForm = (form_name.indexOf(searchFormNameValue.toLowerCase()) > -1);
        }

        return matchName && matchForm;
      });

      this.setState({docs: filtered});
    } else {
      this.setState({docs: docsList});
    }
  }

  _filter() {
    this._doFilter(this.props.docs);
  }

  _viewTypeButton() {
    var label = (this.props.viewType === 'envelope') ? 'Postpone Selected' : 'Move Selected to Envelope';
    return (
      <a className="btn btn-link blue-text" href="#" onClick={this.props.actionCallback.bind(this, this.state.selected)} role="button">
        <i className="sprite sprite-clock" />{label}
      </a>
    );
  }

  render() {
    return (
        <div>
          <div className="row buttons-row">
            <div className="col-md-6 pull-left options-container">
              <div className="col-md-3">
                <a className="btn btn-link blue-text" href="#" onClick={this._selectAll.bind(this)} role="button">
                  <i className="sprite sprite-selected" />Select All
                </a>
              </div>
              <div className="col-md-3">
                <a className="btn btn-link blue-text" href="#" onClick={this._unSelectAll.bind(this)} role="button">
                  <i className="sprite sprite-deselect" />Un-Select All
                </a>
              </div>
              <div className="col-md-4">
                {this._viewTypeButton()}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="row search-form">
                <div className="col-md-4">
                  <div className="form-group">
                    <label>Form</label>
                    <input type="text" className="form-control" value={this.state.formNameInputVal}
                           onChange={this._handleFormNameChange.bind(this)} placeholder="Form Name"
                           onKeyUp={this._filter.bind(this)} />
                  </div>
                 </div>
                 <div className="col-md-offset-1 col-md-4">
                    <div className="form-group">
                      <label>Completed for</label>
                      <input type="text" className="form-control" value={this.state.nameInputVal}
                             onChange={this._handleNameChange.bind(this)} placeholder="Name"
                             onKeyUp={this._filter.bind(this)} />
                    </div>
                 </div>
                 <div className="col-md-3">
                    <span>Click on row to select</span>
                 </div>
                </div>
                <div className="row">
                    <SortableDocList {...this.props} onClick={this._selectItem.bind(this)} selected={this.state.selected}
                                                     docs={this.state.docs ? this.state.docs : this.props.docs} />
                </div>
            </div>
          </div>
        </div>
    );
  }
}

DocumentList.propTypes = {
  docs: React.PropTypes.array,
  actionCallback: React.PropTypes.func,
  viewType: React.PropTypes.string
};

module.exports = DocumentList;
