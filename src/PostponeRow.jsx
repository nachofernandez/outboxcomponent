import React from 'react';

class PostponeRow extends React.Component{
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <li className="row postponed-row">
        <div className="col-md-4 first-col">
          <i className="sprite sprite-clock" />
          <span>Postponed</span>
        </div>
        <div className="col-md-4 middle-col">
          <a className="btn btn-link" href="#" role="button"
             onClick={this.props.showPostponeModal.bind(this, this.props.destination)}
             data-toggle="modal" data-target="#postponedModal">
            {this.props.destination.postpone.length}
            <i className="sprite sprite-sheet" />
          </a>
        </div>
        <div className="col-md-4 last-col"></div>
      </li>
    );
  }
}

PostponeRow.propTypes = {
  destination: React.PropTypes.object,
  showPostponeModal: React.PropTypes.func
};

module.exports = PostponeRow;
