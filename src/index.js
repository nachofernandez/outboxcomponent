import './main.css';

import React from 'react';
import ReactDOM from 'react-dom';
import Outbox from './Outbox.jsx';
import Tabs from 'react-bootstrap/lib/Tabs';
import Tab from 'react-bootstrap/lib/Tab';
import Button from 'react-bootstrap/lib/Button';
import EnvelopeModal from './EnvelopeModal.jsx';
import PostponeModal from './PostponeModal.jsx';

let destinations = [
  {id: '1', name: 'destination 1', email: 'destination1@gmail.com', docs: 40, envelopes: [
    {id:'11', name: 'envelope1', sentDate: null, docs: [
                                        {id:'11', envelopeId: '11', form_name: 'Anthem PPO 1345 From for Eric Kish', name:'Eric Kish'},
                                        {id:'12', envelopeId: '11', form_name: 'Anthem PPO 1345 From for Ignacio', name:'Ignacio Fernandez'}
                                      ]
    },
    {id:'12', name: 'envelope2', sentDate: '2016-01-08T12:48:39+00:00', docs: [{id:'21', envelopeId: '12', form_name: 'Anthem PPO 1345 From for Eric Kish', name:'Eric Kish'}] }
  ],
    postpone: []
  },
  {id: '2', name: 'destination 2', email: 'destination2@gmail.com', docs: 30, envelopes: [
    {id:'21', name: 'envelope1', sentDate: null, docs: [
                                        {id:'31', envelopeId: '21', form_name: 'Anthem PPO 1345 From for Eric Kish', name:'Eric Kish'},
                                        {id:'32', envelopeId: '21', form_name: 'Anthem PPO 1345 From for Ignacio', name:'Ignacio Fernandez'}
                                      ]
    },
    {id:'22', name: 'envelope2', sentDate: null, docs: [
                                        {id:'41', envelopeId: '22', form_name: 'Anthem PPO 1345 From for John Lennon', name:'John Lennon'},
                                        {id:'42', envelopeId: '22', form_name: 'Anthem PPO 1345 From for Eric Clapton', name:'Eric Clapton'}
    ]}
  ],
    postpone: []
  }
];

class App extends React.Component{
  constructor(props) {
    super(props);
    this.state = {destinationsList: destinations, openModal: false,
      openPostponeModal: false, envelope: null, destination: null};
  }

  _removeDestination(destination) {
    var destinationsTemp = this.state.destinationsList;

    destinationsTemp = _.without(destinationsTemp, destination);

    this.setState({destinationsList: destinationsTemp});
  }

  _addDestination() {
    var destinationsTemp = this.state.destinationsList;
    destinationsTemp.push({id: Math.random().toString(36).substring(7), name: 'new destination', email: 'new@gmail.com', docs: 0, postpone: [], envelopes: []});
    this.setState({destinationsList: destinationsTemp});
  }

  _deleteEnvelope(destination, envelope) {
    //Delete envelope from the list

    let destinationList = this.state.destinationsList;
    let tempDestination = _.findWhere(destinationList, destination); //Find destination
    let tempEnvelope = _.findWhere(tempDestination.envelopes, envelope); //Find Envelope
    tempDestination.envelopes = _.without(tempDestination.envelopes, tempEnvelope);
    this.setState({destinationsList: destinationList});
  }

  _updateEnvelope(destination, envelope) {
    let destinationList = this.state.destinationsList;
    let tempDestination = _.findWhere(destinationList, destination); //Find destination
    let tempEnvelope = _.findWhere(tempDestination.envelopes, {id: envelope.id}); //Find Envelope
    tempEnvelope.name = envelope.name;

    this.setState({destinationsList: destinationList});
  }

  _addEnvelope(destination) {
    let destinationList = this.state.destinationsList;
    let tempDestination = _.findWhere(destinationList, destination); //Find destination

    tempDestination.envelopes.push({id:Math.random().toString(36).substring(7), name: 'Envelope', sentDate: null, docs: []});

    this.setState({destinationsList: destinationList});
  }

  _sendEnvelope(destination, envelope) {
    let destinationList = this.state.destinationsList;
    let tempDestination = _.findWhere(destinationList, destination); //Find destination
    let tempEnvelope = _.findWhere(tempDestination.envelopes, envelope); //Find Envelope
    tempEnvelope.sentDate = moment().utc().format();

    this.setState({destinationsList: destinationList});
  }

  _showEnvelopeModal(envelope, destination) {
    this.setState({openModal: true, envelope: envelope, destination: destination});
  }

  _hideEnvelopeModal() {
    this.setState({openModal: false, envelope: null, destination: null});
  }

  _showPostponeModal(destination) {
    this.setState({openPostponeModal: true, destination: destination});
  }

  _hidePostponeModal() {
    this.setState({openPostponeModal: false, destination: null});
  }

  //Move document from envelope to postpone list
  _postponeDocument(document) {
    let destinationList = this.state.destinationsList;
    let tempDestination = this.state.destination;
    let tempEnvelope = this.state.envelope;

    tempEnvelope.docs = _.without(tempEnvelope.docs, document); //Remove doc from envelope
    tempDestination.postpone.push(document); //Add to postpone list

    this.setState({destinationsList: destinationList, destination: tempDestination, envelope: tempEnvelope})
  }

  //Remove document from Envelope
  _removeDocument(document) {
    let destinationList = this.state.destinationsList;
    let tempDestination = this.state.destination;
    let tempEnvelope = this.state.envelope;

    tempEnvelope.docs = _.without(tempEnvelope.docs, document); //Remove doc from envelope

    this.setState({destinationsList: destinationList, destination: tempDestination, envelope: tempEnvelope})
  }

  _removeDocumentFromPostpone(document) {
    let destinationList = this.state.destinationsList;
    let tempDestination = this.state.destination;

    tempDestination.postpone = _.without(tempDestination.postpone, document); //Delete doc

    this.setState({destinationsList: destinationList, destination: tempDestination})
  }

  _moveDocument(document) {
    let destinationList = this.state.destinationsList;
    let tempDestination = this.state.destination;
    let tempEnvelope = _.findWhere(tempDestination.envelopes, {id: document.envelopeId});
    tempEnvelope.docs.push(document);

    tempDestination.postpone = _.without(tempDestination.postpone, document); //Remove doc from postpone

    this.setState({destinationsList: destinationList, destination: tempDestination})
  }

  //Could be improved based on the data structure
  _sortCallback(event, envelope, destination) {
    let destinationList = this.state.destinationsList;
    let tempDestination = _.findWhere(destinationList, destination); //Find destination
    let tempEnvelope = _.findWhere(tempDestination.envelopes, envelope); //Find Envelope

    tempEnvelope.docs.splice(event.newIndex, 0, tempEnvelope.docs.splice(event.oldIndex, 1)[0]);

    this.setState({destinationsList: destinationList, destination: tempDestination});
  }

  _sortPostponedCallback(event, destination) {
    let destinationList = this.state.destinationsList;
    let tempDestination = _.findWhere(destinationList, destination); //Find destination

    tempDestination.postpone.splice(event.newIndex, 0, tempDestination.postpone.splice(event.oldIndex, 1)[0]);

    this.setState({destinationsList: destinationList, destination: tempDestination});
  }

  render() {
    return (
      <div>
        <EnvelopeModal envelope={this.state.envelope} destination={this.state.destination} sortCallback={this._sortCallback.bind(this)}
                       postponeDocument={this._postponeDocument.bind(this)} removeDocument={this._removeDocument.bind(this)}
                       show={this.state.openModal} onHide={this._hideEnvelopeModal.bind(this)} />

        <PostponeModal destination={this.state.destination} show={this.state.openPostponeModal} sortCallback={this._sortPostponedCallback.bind(this)}
                       moveDocument={this._moveDocument.bind(this)} removeDocument={this._removeDocumentFromPostpone.bind(this)}
                       onHide={this._hidePostponeModal.bind(this)} />

        <div className="container">
          <div className="row title-container">
            <div className="col-sm-4">
              <h3>Secure Email Outbox</h3>
            </div>
            <div className="col-sm-4 col-md-offset-1 pull-right">
              <Button bsStyle="success" className="btn-green" onClick={this._addDestination.bind(this)}>+ Add Destination</Button>
            </div>
          </div>

          <Tabs defaultActiveKey={1}>
            <Tab eventKey={1} title="Outbox">
              <Outbox outBoxView destinationsList={this.state.destinationsList} onSendEnvelope={this._sendEnvelope.bind(this)}
                      onUpdateEnvelope={this._updateEnvelope.bind(this)} onDeleteEnvelope={this._deleteEnvelope.bind(this)}
                      showEnvelopeModal={this._showEnvelopeModal.bind(this)} showPostponeModal={this._showPostponeModal.bind(this)}
                      removeDestination={this._removeDestination.bind(this)} addEnvelope={this._addEnvelope.bind(this)} />
            </Tab>
            <Tab eventKey={2} title="Sent">
              <Outbox destinationsList={this.state.destinationsList} onSendEnvelope={this._sendEnvelope.bind(this)}
                      onUpdateEnvelope={this._updateEnvelope.bind(this)} onDeleteEnvelope={this._deleteEnvelope.bind(this)}
                      showEnvelopeModal={this._showEnvelopeModal.bind(this)} showPostponeModal={this._showPostponeModal.bind(this)}
                      removeDestination={this._removeDestination.bind(this)} addEnvelope={this._addEnvelope.bind(this)} />
            </Tab>
          </Tabs>

        </div>
      </div>
    );
  }
}


ReactDOM.render(<App destinationsList={destinations}/>, document.getElementById('outbox'));
