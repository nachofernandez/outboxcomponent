import React from 'react';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import InlineEdit from 'react-edit-inline';


class Envelope extends React.Component{
  constructor(props) {
    super(props);
  }

  dataChanged(data) {
    let envelope = this.props.envelope;
    envelope.name = data.name;
    this.props.updateCallback(envelope);
  }

  _sendEnvelope() {
      this.props.sendCallback(this.props.envelope);
  }

  _deleteEnvelope() {
      this.props.deleteCallback(this.props.envelope);
  }

  _getDuration() {
    var sentDate = moment.utc(this.props.envelope.sentDate);
    var today = moment().utc();
    var duration = moment.duration(sentDate.diff(today));
    return duration.humanize(true);
  }

  _renderBody() {
    if (this.props.sent) {
      return (
        <div className="row">
          <div className="col-md-3 first-col">
            <i className="sprite sprite-envelope" />
            <span>{this.props.envelope.name}</span>
          </div>
          <div className="col-md-3 middle-col">
            <a className="btn btn-link" href="#" role="button" data-toggle="modal" data-target="#envelopeModal">
              {this.props.envelope.docs.length}
              <i className="sprite sprite-sheet" />
            </a>
          </div>
          <div className="col-md-3">
            <span>Sent</span><span className="duration">{this._getDuration()}</span>
          </div>
          <div className="col-md-3 clock-col">
            <i className="sprite sprite-clock" />
          </div>
        </div>
      );
    } else {
      return (
        <div className="row">
          <div className="col-md-4 first-col">
            <i className="sprite sprite-envelope" />
            <InlineEdit className="editable-click" activeClassName="name-input" text={this.props.envelope.name} paramName="name" change={this.dataChanged.bind(this)}   />
          </div>
          <div className="col-md-4 middle-col">
            <a className="btn btn-link envelope-btn" href="#" onClick={this.props.showEnvelopeModal.bind(this, this.props.envelope, this.props.destination)}
               role="button" data-toggle="modal" data-target="#envelopeModal">
              {this.props.envelope.docs.length}
              <i className="sprite sprite-sheet" />
            </a>
          </div>
          <div className="col-md-3 last-col">
            <DropdownButton bsStyle="link" title="Actions" id={'dropId_'+this.props.envelope.id}>
              <MenuItem className="send-btn" onSelect={this._sendEnvelope.bind(this)}>Send</MenuItem>
              <MenuItem className="delete-btn" onSelect={this._deleteEnvelope.bind(this)}>Delete Envelope</MenuItem>
            </DropdownButton>
          </div>
        </div>
      );
    }
  }

  render() {
    return (
      <li className="envelope-header">
        {this._renderBody()}
      </li>
    );
  }
}


Envelope.propTypes = {
  envelope: React.PropTypes.object,
  destination: React.PropTypes.object,
  sendCallback: React.PropTypes.func,
  deleteCallback: React.PropTypes.func,
  updateCallback: React.PropTypes.func,
  showEnvelopeModal: React.PropTypes.func,
  sent: React.PropTypes.bool
};

module.exports = Envelope;
