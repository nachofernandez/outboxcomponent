import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import Modal from 'react-bootstrap/lib/Modal';
import DocumentList from './DocumentList.jsx';

class PostponeModal extends React.Component{
  constructor(props) {
    super(props);
  }

  _moveSelected(selected) {
      if (_.isEqual(selected, {type: 'all'})) {
        this.props.destination.postpone.forEach((doc) => {
          this.props.moveDocument(doc);
        });
      } else {
        this.props.moveDocument(selected);
      }
  }

  _onHide() {
    this.setState({selected: null});
    this.props.onHide();
  }

  _handleSort(event) {
    this.props.sortCallback(event, this.props.destination);
  }

  render() {
    return (
      <Modal {...this.props} dialogClassName="envelope-modal" onHide={this._onHide.bind(this)}>
        <Modal.Header closeButton>
          <Modal.Title>Postponed Documents</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <div className="row">
            <div className="col-md-4">
              <i className="sprite sprite-envelope" />
              <a className="btn btn-link" href="#" role="button">Application forms</a><br />
              <h3>Total Documents Postponed: {this.props.destination ? this.props.destination.postpone.length : ''}</h3>
            </div>
            <div className="col-md-4 middle-col">
              <span>{this.props.destination ? this.props.destination.name : ''}</span><br />
              <label>({this.props.destination ? this.props.destination.email : ''})</label>
            </div>
            <div className="col-md-4 last-col">
              <button className="btn btn-success btn-green" type="button">+ Add Document</button>
            </div>
          </div>
          {this.props.destination ? <DocumentList {...this.props} viewType="postpone" onSort={this._handleSort.bind(this)}
                                                                  actionCallback={this._moveSelected.bind(this)}
                                                                  docs={this.props.destination.postpone} /> : ''}
        </Modal.Body>
      </Modal>
    );
  }
}


PostponeModal.propTypes = {
  destination: React.PropTypes.object,
  onHide: React.PropTypes.func,
  moveDocument: React.PropTypes.func,
  sortCallback: React.PropTypes.func
};

module.exports = PostponeModal;
