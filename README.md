## Basic Usage

* Install dependencies **npm install**
* Testing - **npm test** - Runs Karma/Mocha/Chai/Phantom. 
* Developing - **npm start** - Runs the development server at *localhost:8080* and use Hot Module Replacement. You can override the default host and port through env (`HOST`, `PORT`).
* Generate dist file **npm run dist-min**